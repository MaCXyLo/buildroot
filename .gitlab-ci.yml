# Configuration for Gitlab-CI.
# Builds appear on https://gitlab.com/buildroot.org/buildroot/pipelines
# The .gitlab-ci.yml file is generated from .gitlab-ci.yml.in.
# It needs to be regenerated every time a defconfig is added, using
# "make .gitlab-ci.yml".

image: registry.gitlab.com/macxylo/buildroot-base:master

.check_base:
    except:
        - /^.*-.*_defconfig$/
        - /^.*-tests\..*$/

check-DEVELOPERS:
    extends: .check_base
    # get-developers should print just "No action specified"; if it prints
    # anything else, it's a parse error.
    # The initial ! is removed by YAML so we need to quote it.
    script:
        - "! utils/get-developers | grep -v 'No action specified'"

check-flake8:
    extends: .check_base
    before_script:
        # Help flake8 to find the Python files without .py extension.
        - find * -type f -name '*.py' > files.txt
        - find * -type f -print0 | xargs -0 file | grep 'Python script' | cut -d':' -f1 >> files.txt
        - sort -u files.txt | tee files.processed
    script:
        - python -m flake8 --statistics --count --max-line-length=132 $(cat files.processed)
    after_script:
        - wc -l files.processed

.check-gitlab-ci.yml:
    extends: .check_base
    script:
        - mv .gitlab-ci.yml .gitlab-ci.yml.orig
        - make .gitlab-ci.yml
        - diff -u .gitlab-ci.yml.orig .gitlab-ci.yml

check-package:
    extends: .check_base
    script:
        - make check-package

.defconfig_base:
    script:
#        - echo "Configure Buildroot for ${DEFCONFIG_NAME}"
        - mkdir hai
        - export BR2_DL_DIR="./hai"
        - make ${DEFCONFIG_NAME}
#        - ls -al && pwd && ls -al /package/
#        - cd /package/collectd/
#        - sed -i 's/COLLECTD_CONF_OPTS += \\/COLLECTD_CONF_OPTS += --enable-static \\/g' collectd.mk
#        - cd -
#        - echo "BR2_PACKAGE_COLLECTD=y" >> .config
#        - make source
#        - cd output/build/collectd-*/
        
#        - echo "BR2_STATIC_LIBS=y" >> .config
        
#        - echo "BR2_BINFMT_FLAT=y" >> .config
#        - echo 'Build buildroot'
        - |
            make collectd-build BR2_mips=y > >(tee build.log |grep '>>>') 2>&1 || {
                echo 'Failed build last output'
                tail -200 build.log
                exit 1
            }
#        - make collectd-source
#        - make collectd-patch
#        - cd output/build/collectd-5.9.0/ && make
        - file output/build/collectd-5.9.0/collectd
    artifacts:
        when: always
        expire_in: 2 weeks
        paths:
            - .config
            - build.log
            - output/build/
            - ./hai/
#            - output/build/collectd-5.9.0/
#            - output/images/
#            - output/build/build-time.log
#            - output/build/packages-file-list.txt
#            - output/build/*/.config

.defconfig:
    extends: .defconfig_base
    # Running the defconfigs for every push is too much, so limit to
    # explicit triggers through the API.
    only:
        - web
        - tags
        - /-defconfigs$/
    before_script:
        - DEFCONFIG_NAME=${CI_JOB_NAME}

one-defconfig:
    extends: .defconfig_base
    only:
        - /^.*-.*_defconfig$/
    before_script:
        - DEFCONFIG_NAME=$(echo ${CI_COMMIT_REF_NAME} | sed -e 's,^.*-,,g')

.runtime_test_base:
    # Keep build directories so the rootfs can be an artifact of the job. The
    # runner will clean up those files for us.
    # Multiply every emulator timeout by 10 to avoid sporadic failures in
    # elastic runners.
    script:
        - echo "Starting runtime test ${TEST_CASE_NAME}"
        - ./support/testing/run-tests -o test-output/ -d test-dl/ -k --timeout-multiplier 10 ${TEST_CASE_NAME}
    artifacts:
        when: always
        expire_in: 2 weeks
        paths:
            - test-output/*.log
            - test-output/*/.config
            - test-output/*/images/*

.runtime_test:
    extends: .runtime_test_base
    # Running the runtime tests for every push is too much, so limit to
    # explicit triggers through the API.
    only:
        - triggers
        - tags
        - /-runtime-tests$/
    before_script:
        - TEST_CASE_NAME=${CI_JOB_NAME}

one-runtime_test:
    extends: .runtime_test_base
    only:
        - /^.*-tests\..*$/
    before_script:
        - TEST_CASE_NAME=$(echo ${CI_COMMIT_REF_NAME} | sed -e 's,^.*-,,g')
        
qemu_mips32r2_malta_defconfig: { extends: .defconfig }
# qemu_mips32r2el_malta_defconfig: { extends: .defconfig }
# qemu_mips32r6_malta_defconfig: { extends: .defconfig }
# qemu_mips32r6el_malta_defconfig: { extends: .defconfig }
# qemu_mips64_malta_defconfig: { extends: .defconfig }
# qemu_mips64el_malta_defconfig: { extends: .defconfig }
# qemu_mips64r6_malta_defconfig: { extends: .defconfig }
# qemu_mips64r6el_malta_defconfig: { extends: .defconfig }
tests.boot.test_atf.TestATFAllwinner: { extends: .runtime_test }
tests.boot.test_atf.TestATFMarvell: { extends: .runtime_test }
tests.boot.test_atf.TestATFVexpress: { extends: .runtime_test }
tests.core.test_file_capabilities.TestFileCapabilities: { extends: .runtime_test }
tests.core.test_hardening.TestFortifyConserv: { extends: .runtime_test }
tests.core.test_hardening.TestFortifyNone: { extends: .runtime_test }
tests.core.test_hardening.TestRelro: { extends: .runtime_test }
tests.core.test_hardening.TestRelroPartial: { extends: .runtime_test }
tests.core.test_hardening.TestSspNone: { extends: .runtime_test }
tests.core.test_hardening.TestSspStrong: { extends: .runtime_test }
tests.core.test_post_scripts.TestPostScripts: { extends: .runtime_test }
tests.core.test_root_password.TestRootPassword: { extends: .runtime_test }
tests.core.test_rootfs_overlay.TestRootfsOverlay: { extends: .runtime_test }
tests.core.test_timezone.TestGlibcAllTimezone: { extends: .runtime_test }
tests.core.test_timezone.TestGlibcNonDefaultLimitedTimezone: { extends: .runtime_test }
tests.core.test_timezone.TestNoTimezone: { extends: .runtime_test }
tests.download.test_git.TestGitHash: { extends: .runtime_test }
tests.download.test_git.TestGitRefs: { extends: .runtime_test }
tests.fs.test_ext.TestExt2: { extends: .runtime_test }
tests.fs.test_ext.TestExt2r1: { extends: .runtime_test }
tests.fs.test_ext.TestExt3: { extends: .runtime_test }
tests.fs.test_ext.TestExt4: { extends: .runtime_test }
tests.fs.test_f2fs.TestF2FS: { extends: .runtime_test }
tests.fs.test_iso9660.TestIso9660Grub2External: { extends: .runtime_test }
tests.fs.test_iso9660.TestIso9660Grub2ExternalCompress: { extends: .runtime_test }
tests.fs.test_iso9660.TestIso9660Grub2Internal: { extends: .runtime_test }
tests.fs.test_iso9660.TestIso9660SyslinuxExternal: { extends: .runtime_test }
tests.fs.test_iso9660.TestIso9660SyslinuxExternalCompress: { extends: .runtime_test }
tests.fs.test_iso9660.TestIso9660SyslinuxInternal: { extends: .runtime_test }
tests.fs.test_jffs2.TestJffs2: { extends: .runtime_test }
tests.fs.test_squashfs.TestSquashfs: { extends: .runtime_test }
tests.fs.test_ubi.TestUbi: { extends: .runtime_test }
tests.fs.test_yaffs2.TestYaffs2: { extends: .runtime_test }
tests.init.test_busybox.TestInitSystemBusyboxRo: { extends: .runtime_test }
tests.init.test_busybox.TestInitSystemBusyboxRoNet: { extends: .runtime_test }
tests.init.test_busybox.TestInitSystemBusyboxRw: { extends: .runtime_test }
tests.init.test_busybox.TestInitSystemBusyboxRwNet: { extends: .runtime_test }
tests.init.test_none.TestInitSystemNone: { extends: .runtime_test }
tests.init.test_systemd.TestInitSystemSystemdRoFull: { extends: .runtime_test }
tests.init.test_systemd.TestInitSystemSystemdRoIfupdown: { extends: .runtime_test }
tests.init.test_systemd.TestInitSystemSystemdRoNetworkd: { extends: .runtime_test }
tests.init.test_systemd.TestInitSystemSystemdRwFull: { extends: .runtime_test }
tests.init.test_systemd.TestInitSystemSystemdRwIfupdown: { extends: .runtime_test }
tests.init.test_systemd.TestInitSystemSystemdRwNetworkd: { extends: .runtime_test }
tests.package.test_atop.TestAtop: { extends: .runtime_test }
tests.package.test_docker_compose.TestDockerCompose: { extends: .runtime_test }
tests.package.test_dropbear.TestDropbear: { extends: .runtime_test }
tests.package.test_glxinfo.TestGlxinfo: { extends: .runtime_test }
tests.package.test_ipython.TestIPythonPy3: { extends: .runtime_test }
tests.package.test_lpeg.TestLuaLPeg: { extends: .runtime_test }
tests.package.test_lpeg.TestLuajitLPeg: { extends: .runtime_test }
tests.package.test_lsqlite3.TestLuaLsqlite3: { extends: .runtime_test }
tests.package.test_lsqlite3.TestLuajitLsqlite3: { extends: .runtime_test }
tests.package.test_lua.TestLua: { extends: .runtime_test }
tests.package.test_lua.TestLuajit: { extends: .runtime_test }
tests.package.test_lua_curl.TestLuaLuacURL: { extends: .runtime_test }
tests.package.test_lua_curl.TestLuajitLuacURL: { extends: .runtime_test }
tests.package.test_lua_http.TestLuaHttp: { extends: .runtime_test }
tests.package.test_lua_http.TestLuajitHttp: { extends: .runtime_test }
tests.package.test_lua_utf8.TestLuaUtf8: { extends: .runtime_test }
tests.package.test_lua_utf8.TestLuajitUtf8: { extends: .runtime_test }
tests.package.test_luaexpat.TestLuaLuaExpat: { extends: .runtime_test }
tests.package.test_luaexpat.TestLuajitLuaExpat: { extends: .runtime_test }
tests.package.test_luafilesystem.TestLuaLuaFileSystem: { extends: .runtime_test }
tests.package.test_luafilesystem.TestLuajitLuaFileSystem: { extends: .runtime_test }
tests.package.test_luaossl.TestLuaLuaossl: { extends: .runtime_test }
tests.package.test_luaossl.TestLuajitLuaossl: { extends: .runtime_test }
tests.package.test_luaposix.TestLuaLuaPosix: { extends: .runtime_test }
tests.package.test_luaposix.TestLuajitLuaPosix: { extends: .runtime_test }
tests.package.test_luasec.TestLuaLuaSec: { extends: .runtime_test }
tests.package.test_luasec.TestLuajitLuaSec: { extends: .runtime_test }
tests.package.test_luasocket.TestLuaLuaSocket: { extends: .runtime_test }
tests.package.test_luasocket.TestLuajitLuaSocket: { extends: .runtime_test }
tests.package.test_luasyslog.TestLuaLuasyslog: { extends: .runtime_test }
tests.package.test_luasyslog.TestLuajitLuasyslog: { extends: .runtime_test }
tests.package.test_openjdk.TestOpenJdk: { extends: .runtime_test }
tests.package.test_perl.TestPerl: { extends: .runtime_test }
tests.package.test_perl_class_load.TestPerlClassLoad: { extends: .runtime_test }
tests.package.test_perl_dbd_mysql.TestPerlDBDmysql: { extends: .runtime_test }
tests.package.test_perl_encode_detect.TestPerlEncodeDetect: { extends: .runtime_test }
tests.package.test_perl_gdgraph.TestPerlGDGraph: { extends: .runtime_test }
tests.package.test_perl_io_socket_multicast.TestPerlIOSocketMulticast: { extends: .runtime_test }
tests.package.test_perl_io_socket_ssl.TestPerlIOSocketSSL: { extends: .runtime_test }
tests.package.test_perl_libwww_perl.TestPerllibwwwperl: { extends: .runtime_test }
tests.package.test_perl_mail_dkim.TestPerlMailDKIM: { extends: .runtime_test }
tests.package.test_perl_x10.TestPerlX10: { extends: .runtime_test }
tests.package.test_perl_xml_libxml.TestPerlXMLLibXML: { extends: .runtime_test }
tests.package.test_prosody.TestProsodyLua51: { extends: .runtime_test }
tests.package.test_prosody.TestProsodyLuajit: { extends: .runtime_test }
tests.package.test_python.TestPython2: { extends: .runtime_test }
tests.package.test_python.TestPython3: { extends: .runtime_test }
tests.package.test_python_argh.TestPythonPy2Argh: { extends: .runtime_test }
tests.package.test_python_argh.TestPythonPy3Argh: { extends: .runtime_test }
tests.package.test_python_attrs.TestPythonPy2Attrs: { extends: .runtime_test }
tests.package.test_python_attrs.TestPythonPy3Attrs: { extends: .runtime_test }
tests.package.test_python_autobahn.TestPythonPy2Autobahn: { extends: .runtime_test }
tests.package.test_python_autobahn.TestPythonPy3Autobahn: { extends: .runtime_test }
tests.package.test_python_automat.TestPythonPy2Automat: { extends: .runtime_test }
tests.package.test_python_automat.TestPythonPy3Automat: { extends: .runtime_test }
tests.package.test_python_bitstring.TestPythonPy2Bitstring: { extends: .runtime_test }
tests.package.test_python_bitstring.TestPythonPy3Bitstring: { extends: .runtime_test }
tests.package.test_python_cbor.TestPythonPy2Cbor: { extends: .runtime_test }
tests.package.test_python_cbor.TestPythonPy3Cbor: { extends: .runtime_test }
tests.package.test_python_click.TestPythonPy2Click: { extends: .runtime_test }
tests.package.test_python_click.TestPythonPy3Click: { extends: .runtime_test }
tests.package.test_python_constantly.TestPythonPy2Constantly: { extends: .runtime_test }
tests.package.test_python_constantly.TestPythonPy3Constantly: { extends: .runtime_test }
tests.package.test_python_crossbar.TestPythonPy3Crossbar: { extends: .runtime_test }
tests.package.test_python_cryptography.TestPythonPy2Cryptography: { extends: .runtime_test }
tests.package.test_python_cryptography.TestPythonPy3Cryptography: { extends: .runtime_test }
tests.package.test_python_incremental.TestPythonPy2Incremental: { extends: .runtime_test }
tests.package.test_python_incremental.TestPythonPy3Incremental: { extends: .runtime_test }
tests.package.test_python_passlib.TestPythonPy2Passlib: { extends: .runtime_test }
tests.package.test_python_passlib.TestPythonPy3Passlib: { extends: .runtime_test }
tests.package.test_python_pexpect.TestPythonPy2Pexpect: { extends: .runtime_test }
tests.package.test_python_pexpect.TestPythonPy3Pexpect: { extends: .runtime_test }
tests.package.test_python_pynacl.TestPythonPy2Pynacl: { extends: .runtime_test }
tests.package.test_python_pynacl.TestPythonPy3Pynacl: { extends: .runtime_test }
tests.package.test_python_pyyaml.TestPythonPy2Pyyaml: { extends: .runtime_test }
tests.package.test_python_pyyaml.TestPythonPy3Pyyaml: { extends: .runtime_test }
tests.package.test_python_service_identity.TestPythonPy2ServiceIdentity: { extends: .runtime_test }
tests.package.test_python_service_identity.TestPythonPy3ServiceIdentity: { extends: .runtime_test }
tests.package.test_python_subprocess32.TestPythonPy2Subprocess32: { extends: .runtime_test }
tests.package.test_python_treq.TestPythonPy2Treq: { extends: .runtime_test }
tests.package.test_python_treq.TestPythonPy3Treq: { extends: .runtime_test }
tests.package.test_python_twisted.TestPythonPy2Twisted: { extends: .runtime_test }
tests.package.test_python_twisted.TestPythonPy3Twisted: { extends: .runtime_test }
tests.package.test_python_txaio.TestPythonPy2Txaio: { extends: .runtime_test }
tests.package.test_python_txaio.TestPythonPy3Txaio: { extends: .runtime_test }
tests.package.test_python_txtorcon.TestPythonPy2Txtorcon: { extends: .runtime_test }
tests.package.test_python_txtorcon.TestPythonPy3Txtorcon: { extends: .runtime_test }
tests.package.test_python_ubjson.TestPythonPy2Ubjson: { extends: .runtime_test }
tests.package.test_python_ubjson.TestPythonPy3Ubjson: { extends: .runtime_test }
tests.package.test_rings.TestLuaRings: { extends: .runtime_test }
tests.package.test_rings.TestLuajitRings: { extends: .runtime_test }
tests.package.test_rust.TestRust: { extends: .runtime_test }
tests.package.test_rust.TestRustBin: { extends: .runtime_test }
tests.package.test_syslog_ng.TestSyslogNg: { extends: .runtime_test }
tests.toolchain.test_external.TestExternalToolchainBuildrootMusl: { extends: .runtime_test }
tests.toolchain.test_external.TestExternalToolchainBuildrootuClibc: { extends: .runtime_test }
tests.toolchain.test_external.TestExternalToolchainCCache: { extends: .runtime_test }
tests.toolchain.test_external.TestExternalToolchainCtngMusl: { extends: .runtime_test }
tests.toolchain.test_external.TestExternalToolchainLinaroArm: { extends: .runtime_test }
tests.toolchain.test_external.TestExternalToolchainSourceryArmv4: { extends: .runtime_test }
tests.toolchain.test_external.TestExternalToolchainSourceryArmv5: { extends: .runtime_test }
tests.toolchain.test_external.TestExternalToolchainSourceryArmv7: { extends: .runtime_test }
tests.utils.test_check_package.TestCheckPackage: { extends: .runtime_test }
